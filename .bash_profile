#
# .bash_profile
#
# @author Jeff Geerling
# @see .inputrc
#
#source /usr/local/etc/bash_completion.d/git-prompt.sh
if [ -d $(brew --prefix)/etc/bash_completion.d ]; then
source $(brew --prefix)/etc/bash_completion.d/git-completion.bash
source $(brew --prefix)/etc/bash_completion.d/git-prompt.sh
fi
GIT_PS1_SHOWDIRTYSTATE=true

# Nicer prompt.
#export PS1="\[\e[0;32m\]\]\[\] \[\e[1;32m\]\]\t \[\e[0;2m\]\]\w \[\e[0m\]\]\[$\] "
#export PS1="\[\e[0;32m\]\]\u@\[\e[1;32m\]\]\h \[\e[0;2m\]\]\w \[\e[0m\]\]\[$\] "
#PS1='\[\e[0;32m\]\]\u@\[\e[1;32m\]\]\h \[\e[0;2m\]\]\w $(__git_ps1 " (%s)")\[\e[0m\]\]\[$\] '
PS1='\[\e[0;32m\]\]\u@\[\e[1;32m\]\]\h \[\e[0;37m\]\]\w \[\e[0;33m\]\]$(__git_ps1 " (%s)")\[\e[0m\]\]\[$\] '

# Use colors.
export CLICOLOR=1
export LSCOLORS=ExFxCxDxBxegedabagacad

# Custom $PATH with extra locations.
export PATH=$PATH:$HOME/.local/bin:$HOME/bin
#export PATH=/usr/local/bin:/usr/local/sbin:$HOME/bin:/usr/local/git/bin:$HOME/.composer/vendor/bin:$PATH

# Include alias file (if present) containing aliases for ssh, etc.
if [ -f ~/.bash_aliases ]
then
  source ~/.bash_aliases
fi

# Include bashrc file (if present).
if [ -f ~/.bashrc ]
then
  source ~/.bashrc
fi

# Syntax-highlight code for copying and pasting.
# Requires highlight (`brew install highlight`).
function pretty() {
  pbpaste | highlight --syntax=$1 -O rtf | pbcopy
}


# Git upstream branch syncer.
# Usage: gsync master (checks out master, pull upstream, push origin).
#function gsync() {
  #if [[ ! "$1" ]] ; then
      #echo "You must supply a branch."
      #return 0
  #fi
#
  #BRANCHES=$(git branch --list $1)
  #if [ ! "$BRANCHES" ] ; then
     #echo "Branch $1 does not exist."
     #return 0
  #fi

  #git checkout "$1" && \
  #git pull upstream "$1" && \
  #git push origin "$1"
#}

# Python settings.
#export PYTHONPATH="/usr/local/lib/python2.7/site-packages"

# Delete a given line number in the known_hosts file.
knownrm() {
  re='^[0-9]+$'
  if ! [[ $1 =~ $re ]] ; then
    echo "error: line number missing" >&2;
  else
    sed -i '' "$1d" ~/.ssh/known_hosts
  fi
}

eval "$(pyenv init -)"
#
